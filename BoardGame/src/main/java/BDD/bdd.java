
package BDD;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;

/**
 * @author pleta
 */
public class bdd {
   protected static String dbPath;

    public static void setDbPath(String path) {
        bdd.dbPath = String.format("jdbc:sqlite:%s", path);
    }
    
    public static void choixbdd(){
        
       //bdd.setDbPath("Z:\\Java\\boardgames-passion\\gestion_stock.sqlite3");
      
       JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());

	int returnValue = jfc.showOpenDialog(null);

	if (returnValue == JFileChooser.APPROVE_OPTION) {
	File selectedFile = jfc.getSelectedFile();
	System.out.println(selectedFile.getAbsolutePath());
        bdd.setDbPath(selectedFile.getAbsolutePath()); 
        }
    }
    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        Connection connection = DriverManager.getConnection(bdd.dbPath); //se connecter à la base de données
        return connection;
    }

    public static Statement createStatement(Connection connection) throws SQLException {
        return connection.createStatement();
    }

    public static PreparedStatement createPreparedStatement(Connection connection, String query) throws SQLException {
        return connection.prepareStatement(query);
    }

    public static ResultSet executeQuery(Statement statement, String query) throws SQLException {
        return statement.executeQuery(query);
    }
    
    public static ResultSet executePreparedQuery(PreparedStatement preparedStatement) throws SQLException{
        return preparedStatement.executeQuery();
    } 
}


