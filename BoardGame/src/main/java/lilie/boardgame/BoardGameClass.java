/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lilie.boardgame;

import BDD.bdd;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pleta
 */
public class BoardGameClass {
    protected int id;
    protected String title;
    protected String marque;
    protected double price;
    protected String players;
    protected int in_stock;
    
    public BoardGameClass(int id, String title, double price, String players,String marque, int in_stock) {
        this.id = id;
        this.title = title;        
        this.price = price;
        this.players = players;
        this.marque = marque;
        this.in_stock = in_stock;
    }
    
    public static BoardGameClass createFromResultSec(ResultSet resultSet) throws SQLException{
        return new BoardGameClass(
                resultSet.getInt("id"),
                resultSet.getString("title"), 
                resultSet.getDouble("price"),
                resultSet.getString("players"),
                resultSet.getString("marque"),
                resultSet.getInt("in_stock")
                
        );
    }
    
    public static List<BoardGameClass> all() throws SQLException, ClassNotFoundException{
       Connection connection = bdd.getConnection(); 
       Statement statement = bdd.createStatement(connection);
       String query = "SELECT * FROM GestionS"; //récupère la requête pour la base de donnée
       ResultSet resultSet = bdd.executeQuery(statement, query);
       List<BoardGameClass> lignes; //creation de la liste
       lignes = new ArrayList<>(); 
       
       while(resultSet.next()){
            lignes.add(BoardGameClass.createFromResultSec(resultSet));
        }
     
       return lignes;
    }
    
    public Object[] Instance(){
        return new Object[] { this.id, this.title, this.price, this.players,this.marque, this.in_stock };
    }
    public void delete() throws SQLException, ClassNotFoundException{
        Connection con = bdd.getConnection();
        String query = ("DELETE FROM GestionS WHERE id = ?;"); //requête SQL pour supprimer
        PreparedStatement pst = con.prepareStatement(query);
        pst.setInt(1, this.id);
        pst.executeUpdate();
    }
    public void update() throws SQLException, ClassNotFoundException{
        Connection con = bdd.getConnection();
        String query = ("UPDATE FROM GestionS WHERE id = ?;"); //requête SQL pour modifier
        PreparedStatement pst = con.prepareStatement(query);
            //String sql="UPDATE EMPLOYE SET ID = '"+jtitretext.getText()+"',nom='"+tfnomemploye.getText()+"', prenom='"+tfprenomemploye.getText()+"', salaire='"+tfsalaireemploye.getText()+"', age='"+tfageemploye.getText()+"' ";
    
        pst.setInt(1, this.id);
        pst.executeUpdate();        
    }
  
    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getMarque() {
        return marque;
    }

    public double getPrice() {
        return price;
    }

    public String getPlayers() {
        return players;
    }

    public int getInStock() {
        return in_stock;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setPlayers(String players) {
        this.players = players;
    }

    public void setInStock(int in_stock) {
        this.in_stock = in_stock;
    }  
    
}
